(ns clj-gitlab.issues-test
  (:require [clojure.test :refer :all]
            [clj-gitlab.issues :refer :all]
            [vcr-clj.clj-http :refer [with-cassette]]))

(deftest project-issues-test
  (testing "/projects/:id/issues endpoint for authenticated user"
    (with-cassette :project-issues
      (is (seq (project-issues "4202845" {:token (System/getenv "PERSONAL_TOKEN")}))))))

(deftest issues-test
  (testing "/issues endpoint for authenticated user"
    (with-cassette :issues
      (is (seq (issues {:token (System/getenv "PERSONAL_TOKEN")}))))))

(deftest issue-test
  (testing "/projects/:id/issues/:iid endpoint for authenticated user"
    (with-cassette :issue
      (is (=
            (get (issue "4202845" "1" {:token (System/getenv "PERSONAL_TOKEN")}) "title")
            "Open issue")))))

(deftest create-issue-test
  (testing "POST /projects/:id/issues/ endpoint for authenticated user"
    (with-cassette :create-issue
      (is (=
            (get (create-issue "4202845" {:title "Wow much issue"} {:token (System/getenv "PERSONAL_TOKEN")}) "title")
            "Wow much issue")))))

(deftest update-issue-test
  (testing "PUT /projects/:id/issues/:iid endpoint for authenticated user"
    (with-cassette :update-issue
      (is (=
            (get (update-issue "4202845" "4" {:description "Please fix"} {:token (System/getenv "PERSONAL_TOKEN")}) "description")
            "Please fix")))))
