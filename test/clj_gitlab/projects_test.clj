(ns clj-gitlab.projects-test
  (:require [clojure.test :refer :all]
            [clj-gitlab.projects :refer :all]
            [vcr-clj.clj-http :refer [with-cassette]]))

(deftest projects-test
  (testing "/projects endpoint for anonymous user"
    (with-cassette :projects
      (is (seq (projects))))))

(deftest project-test
  (testing "/projects/:id endpoint for anonymous user"
    (with-cassette :project
      (is (= (get (project "4186769") "name") "clj-gitlab")))))

(deftest user-projects-test
  (testing "/users/:user-id/projects endpoint for anonymous user"
    (with-cassette :user-projects
      (is (seq (user-projects "dzaporozhets"))))))

(deftest create-project-test
  (testing "POST /projects endpoint for authenticated user"
    (with-cassette :create-project
      (is (=
            (get (create-project {:name "much-project"} {:token (System/getenv "PERSONAL_TOKEN")}) "path")
            "much-project")))))

(deftest update-project-test
  (testing "PUT /projects/:id endpoint for authenticated user"
    (with-cassette :update-project
      (is (=
            (get (update-project "4202845" {:description "Nice project"} {:token (System/getenv "PERSONAL_TOKEN")}) "description")
            "Nice project")))))

(deftest group-projects-test
         (testing "/groups/:group-id/projects endpoint for anonymous user"
                  (with-cassette :group-projects
                                 (is (seq (group-projects "6543"))))))

