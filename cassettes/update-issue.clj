{:calls [{:arg-key {:query-string nil,
                    :request-method :put,
                    :server-name "gitlab.com",
                    :server-port nil,
                    :uri "/api/v4/projects/4202845/issues/4"},
          :return {:body #vcr-clj/input-stream
                   ["eyJpZCI6NjkyMDA2MiwiaWlkIjo0LCJwcm9qZWN0X2lkIjo0MjAyODQ1LCJ0aXRsZSI6IldvdyB"
                    "tdWNoIGlzc3VlIiwiZGVzY3JpcHRpb24iOiJQbGVhc2UgZml4Iiwic3RhdGUiOiJvcGVuZWQiLC"
                    "JjcmVhdGVkX2F0IjoiMjAxNy0wOS0yMlQxNDoxNjoyNC44OThaIiwidXBkYXRlZF9hdCI6IjIwM"
                    "TctMDktMjJUMTY6MDA6NTguNzI4WiIsImxhYmVscyI6W10sIm1pbGVzdG9uZSI6bnVsbCwiYXNz"
                    "aWduZWVzIjpbXSwiYXV0aG9yIjp7ImlkIjozODUyNywibmFtZSI6IkRvZ2UgRGV2IiwidXNlcm5"
                    "hbWUiOiJkb2dlZGV2Iiwic3RhdGUiOiJhY3RpdmUiLCJhdmF0YXJfdXJsIjoiaHR0cHM6Ly9nbC"
                    "1zdGF0aWMuZ2xvYmFsLnNzbC5mYXN0bHkubmV0L3VwbG9hZHMvLS9zeXN0ZW0vdXNlci9hdmF0Y"
                    "XIvMzg1MjcvZG9nZS5qcGVnIiwid2ViX3VybCI6Imh0dHBzOi8vZ2l0bGFiLmNvbS9kb2dlZGV2"
                    "In0sImFzc2lnbmVlIjpudWxsLCJ1c2VyX25vdGVzX2NvdW50IjowLCJ1cHZvdGVzIjowLCJkb3d"
                    "udm90ZXMiOjAsImR1ZV9kYXRlIjpudWxsLCJjb25maWRlbnRpYWwiOmZhbHNlLCJ3ZWlnaHQiOm"
                    "51bGwsIndlYl91cmwiOiJodHRwczovL2dpdGxhYi5jb20vZG9nZWRldi90ZXN0LWNsaS1naXRsY"
                    "WIvaXNzdWVzLzQiLCJ0aW1lX3N0YXRzIjp7InRpbWVfZXN0aW1hdGUiOjAsInRvdGFsX3RpbWVf"
                    "c3BlbnQiOjAsImh1bWFuX3RpbWVfZXN0aW1hdGUiOm51bGwsImh1bWFuX3RvdGFsX3RpbWVfc3B"
                    "lbnQiOm51bGx9LCJfbGlua3MiOnsic2VsZiI6Imh0dHA6Ly9naXRsYWIuY29tL2FwaS92NC9wcm"
                    "9qZWN0cy80MjAyODQ1L2lzc3Vlcy80Iiwibm90ZXMiOiJodHRwOi8vZ2l0bGFiLmNvbS9hcGkvd"
                    "jQvcHJvamVjdHMvNDIwMjg0NS9pc3N1ZXMvNC9ub3RlcyIsImF3YXJkX2Vtb2ppIjoiaHR0cDov"
                    "L2dpdGxhYi5jb20vYXBpL3Y0L3Byb2plY3RzLzQyMDI4NDUvaXNzdWVzLzQvYXdhcmRfZW1vamk"
                    "iLCJwcm9qZWN0IjoiaHR0cDovL2dpdGxhYi5jb20vYXBpL3Y0L3Byb2plY3RzLzQyMDI4NDUifS"
                    "wic3Vic2NyaWJlZCI6dHJ1ZX0="],
                   :chunked? false,
                   :headers #vcr-clj/clj-http-header-map
                   {"Cache-Control" "max-age=0, private, must-revalidate",
                    "Connection" "close",
                    "Content-Length" "1031",
                    "Content-Type" "application/json",
                    "Date" "Fri, 22 Sep 2017 16:00:59 GMT",
                    "Etag" "W/\"50581fc1dc10a67840f6671776e414a1\"",
                    "RateLimit-Limit" "600",
                    "RateLimit-Observed" "1",
                    "RateLimit-Remaining" "599",
                    "Server" "nginx",
                    "Strict-Transport-Security" "max-age=31536000",
                    "Vary" "Origin",
                    "X-Frame-Options" "SAMEORIGIN",
                    "X-Request-Id" "56376ba9-b192-4763-bd8a-a5f9319cba1c",
                    "X-Runtime" "0.605283"},
                   :length 1031,
                   :protocol-version {:major 1, :minor 1, :name "HTTP"},
                   :reason-phrase "OK",
                   :repeatable? false,
                   :status 200,
                   :streaming? true,
                   :trace-redirects []},
          :var-name "clj-http.core/request"}],
 :recorded-at #inst "2017-09-22T16:00:58.312-00:00"}
