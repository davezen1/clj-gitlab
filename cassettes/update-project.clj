{:calls [{:arg-key {:query-string nil,
                    :request-method :put,
                    :server-name "gitlab.com",
                    :server-port nil,
                    :uri "/api/v4/projects/4202845"},
          :return {:body #vcr-clj/input-stream
                   ["eyJpZCI6NDIwMjg0NSwiZGVzY3JpcHRpb24iOiJOaWNlIHByb2plY3QiLCJkZWZhdWx0X2JyYW5"
                    "jaCI6bnVsbCwidGFnX2xpc3QiOltdLCJzc2hfdXJsX3RvX3JlcG8iOiJnaXRAZ2l0bGFiLmNvbT"
                    "pkb2dlZGV2L3Rlc3QtY2xpLWdpdGxhYi5naXQiLCJodHRwX3VybF90b19yZXBvIjoiaHR0cHM6L"
                    "y9naXRsYWIuY29tL2RvZ2VkZXYvdGVzdC1jbGktZ2l0bGFiLmdpdCIsIndlYl91cmwiOiJodHRw"
                    "czovL2dpdGxhYi5jb20vZG9nZWRldi90ZXN0LWNsaS1naXRsYWIiLCJuYW1lIjoidGVzdC1jbGk"
                    "tZ2l0bGFiIiwibmFtZV93aXRoX25hbWVzcGFjZSI6IkRvZ2UgRGV2IC8gdGVzdC1jbGktZ2l0bG"
                    "FiIiwicGF0aCI6InRlc3QtY2xpLWdpdGxhYiIsInBhdGhfd2l0aF9uYW1lc3BhY2UiOiJkb2dlZ"
                    "GV2L3Rlc3QtY2xpLWdpdGxhYiIsInN0YXJfY291bnQiOjAsImZvcmtzX2NvdW50IjowLCJjcmVh"
                    "dGVkX2F0IjoiMjAxNy0wOS0yMVQxMzoxODoyNC43MzJaIiwibGFzdF9hY3Rpdml0eV9hdCI6IjI"
                    "wMTctMDktMjJUMTQ6MTY6MjUuMTIwWiIsIl9saW5rcyI6eyJzZWxmIjoiaHR0cDovL2dpdGxhYi"
                    "5jb20vYXBpL3Y0L3Byb2plY3RzLzQyMDI4NDUiLCJpc3N1ZXMiOiJodHRwOi8vZ2l0bGFiLmNvb"
                    "S9hcGkvdjQvcHJvamVjdHMvNDIwMjg0NS9pc3N1ZXMiLCJtZXJnZV9yZXF1ZXN0cyI6Imh0dHA6"
                    "Ly9naXRsYWIuY29tL2FwaS92NC9wcm9qZWN0cy80MjAyODQ1L21lcmdlX3JlcXVlc3RzIiwicmV"
                    "wb19icmFuY2hlcyI6Imh0dHA6Ly9naXRsYWIuY29tL2FwaS92NC9wcm9qZWN0cy80MjAyODQ1L3"
                    "JlcG9zaXRvcnkvYnJhbmNoZXMiLCJsYWJlbHMiOiJodHRwOi8vZ2l0bGFiLmNvbS9hcGkvdjQvc"
                    "HJvamVjdHMvNDIwMjg0NS9sYWJlbHMiLCJldmVudHMiOiJodHRwOi8vZ2l0bGFiLmNvbS9hcGkv"
                    "djQvcHJvamVjdHMvNDIwMjg0NS9ldmVudHMiLCJtZW1iZXJzIjoiaHR0cDovL2dpdGxhYi5jb20"
                    "vYXBpL3Y0L3Byb2plY3RzLzQyMDI4NDUvbWVtYmVycyJ9LCJhcmNoaXZlZCI6ZmFsc2UsInZpc2"
                    "liaWxpdHkiOiJwcml2YXRlIiwib3duZXIiOnsiaWQiOjM4NTI3LCJuYW1lIjoiRG9nZSBEZXYiL"
                    "CJ1c2VybmFtZSI6ImRvZ2VkZXYiLCJzdGF0ZSI6ImFjdGl2ZSIsImF2YXRhcl91cmwiOiJodHRw"
                    "czovL2dpdGxhYi5jb20vdXBsb2Fkcy8tL3N5c3RlbS91c2VyL2F2YXRhci8zODUyNy9kb2dlLmp"
                    "wZWciLCJ3ZWJfdXJsIjoiaHR0cHM6Ly9naXRsYWIuY29tL2RvZ2VkZXYifSwicmVzb2x2ZV9vdX"
                    "RkYXRlZF9kaWZmX2Rpc2N1c3Npb25zIjpmYWxzZSwiY29udGFpbmVyX3JlZ2lzdHJ5X2VuYWJsZ"
                    "WQiOnRydWUsImlzc3Vlc19lbmFibGVkIjp0cnVlLCJtZXJnZV9yZXF1ZXN0c19lbmFibGVkIjp0"
                    "cnVlLCJ3aWtpX2VuYWJsZWQiOnRydWUsImpvYnNfZW5hYmxlZCI6dHJ1ZSwic25pcHBldHNfZW5"
                    "hYmxlZCI6dHJ1ZSwic2hhcmVkX3J1bm5lcnNfZW5hYmxlZCI6dHJ1ZSwibGZzX2VuYWJsZWQiOn"
                    "RydWUsImNyZWF0b3JfaWQiOjM4NTI3LCJuYW1lc3BhY2UiOnsiaWQiOjQzODI5LCJuYW1lIjoiZ"
                    "G9nZWRldiIsInBhdGgiOiJkb2dlZGV2Iiwia2luZCI6InVzZXIiLCJmdWxsX3BhdGgiOiJkb2dl"
                    "ZGV2IiwicGFyZW50X2lkIjpudWxsfSwiaW1wb3J0X3N0YXR1cyI6Im5vbmUiLCJpbXBvcnRfZXJ"
                    "yb3IiOm51bGwsImF2YXRhcl91cmwiOm51bGwsIm9wZW5faXNzdWVzX2NvdW50IjozLCJydW5uZX"
                    "JzX3Rva2VuIjoiXzV4dWRZaktBVHVhSEZEdnN1MjYiLCJwdWJsaWNfam9icyI6dHJ1ZSwiY2lfY"
                    "29uZmlnX3BhdGgiOm51bGwsInNoYXJlZF93aXRoX2dyb3VwcyI6W10sIm9ubHlfYWxsb3dfbWVy"
                    "Z2VfaWZfcGlwZWxpbmVfc3VjY2VlZHMiOmZhbHNlLCJyZXF1ZXN0X2FjY2Vzc19lbmFibGVkIjp"
                    "mYWxzZSwib25seV9hbGxvd19tZXJnZV9pZl9hbGxfZGlzY3Vzc2lvbnNfYXJlX3Jlc29sdmVkIj"
                    "pmYWxzZSwicHJpbnRpbmdfbWVyZ2VfcmVxdWVzdF9saW5rX2VuYWJsZWQiOnRydWUsImFwcHJvd"
                    "mFsc19iZWZvcmVfbWVyZ2UiOjB9"],
                   :chunked? false,
                   :headers #vcr-clj/clj-http-header-map
                   {"Cache-Control" "max-age=0, private, must-revalidate",
                    "Connection" "close",
                    "Content-Length" "1989",
                    "Content-Type" "application/json",
                    "Date" "Fri, 22 Sep 2017 16:16:16 GMT",
                    "Etag" "W/\"efbca4debc09a0c126042ae4f848d6b2\"",
                    "RateLimit-Limit" "600",
                    "RateLimit-Observed" "1",
                    "RateLimit-Remaining" "599",
                    "Server" "nginx",
                    "Strict-Transport-Security" "max-age=31536000",
                    "Vary" "Origin",
                    "X-Frame-Options" "SAMEORIGIN",
                    "X-Request-Id" "48fcb3b8-9691-491d-9ca8-61eb2577749d",
                    "X-Runtime" "0.343150"},
                   :length 1989,
                   :protocol-version {:major 1, :minor 1, :name "HTTP"},
                   :reason-phrase "OK",
                   :repeatable? false,
                   :status 200,
                   :streaming? true,
                   :trace-redirects []},
          :var-name "clj-http.core/request"}],
 :recorded-at #inst "2017-09-22T16:16:16.107-00:00"}
